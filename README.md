## Le sujet du script est le suivant.

Sur votre machine virtuelle, créer un script qui permet d'automatiser la synchronisation (commande rsync) de 2 répertoires passés en arguments.

### Le script doit contenir 3 options :
* Affichage d'une aide stockée dans le script.
    * Synchronisation des répertoires.
    * Affichage d'un journal des synchronisations réalisées.
* De plus, il est demandé de :
    * Créer une page dans le manuel.
    * Créer un fichier de configuration associé qui contiendra les répertoires à synchroniser.
    * Permettre de synchroniser dans les deux sens.

#### Fichiers : 
* ``rsync.sh`` :  script a exécuter pour la synchronisation. Instructions données à l'exécution.
* ``irsync.config`` : fichier de configuration contenant deux variables
  * __SOURCE__ = chemin du répertoire source pour la synchronisation
  * __DESTINATION__ = chemin du répertoire destination pour la synchronisation
* ``irsync.man`` : page de manuel pour irsync, lisible grâce à la commande : ``man ./irsync.man``
  * pour installer la page de ce manuel, vous devez éxecuter le script ``rsync.sh`` avec les droits administrateurs puis utiliser la commande ``man 1 irsync``


