#!/bin/bash

MAN_PATH_FOLDER="/usr/local/share/man/man1"
MAN_PATH_FILE="/usr/local/share/man/man1/irsync.1"

function add_man {
    if [ ! -f $MAN_PATH_FILE ]; then
        cp irsync.man $MAN_PATH_FILE
        mandb
    fi
}

if [ ! -d $MAN_PATH_FOLDER ]; then
    mkdir -p $MAN_PATH_FOLDER
    add_man
else
    add_man
fi

RED='\033[0;31m'
CYAN='\033[0;36m'
PURPLE='\033[0;35m'
GREEN="\033[0;32m"
BLUE="\033[0;34m"
LIGHT_BLUE="\033[1;34m"
NC='\033[0m' # no color

HISTORY_FILE="irsync.history"
ERROR_FILE="irsync.error"
CONFIG_FILE="irsync.conf"

function help {
    echo -e "script permettant la synchronisation entre deux répertoires par le biais de la commande ${GREEN}rsync${NC}."
    echo -e "La synchronisation est unidirectionnel si rien n'est précisé, elle permet de synchroniser un dossier ${GREEN}source${NC} avec un dossier ${GREEN}destination${NC}"
    echo -e "Vous pouvez utiliser ces options : "
    echo -e "\t -h (help) pour afficher l'aide de ce script"
    echo -e "\t -s (source) dossier ${GREEN}source${NC} à synchroniser"
    echo -e "\t -d (destination) dossier ${GREEN}destination${NC} à synchroniser"
    echo -e "\t -p (print) afficher l'historique des synchronisations"
    echo -e "\t -b (bidirectional) permet de faire une synchronisation bidirectionnel entre la source et la destination"
    echo -e "\t -r (remove) supprime le fichier contenant l'historique des synchronisation"
    echo -e "\t -c (configuration) utilise le fichier irsync.conf pour définir les répertoires source et destination"
    echo 
    echo -e "pour installer la page de manuel, exécutez le avec les droits administrateurs puis utiliser la commande man 1 irsync"
}

# create history file who will be used to store file synchronized with rsync
# if file already exists, empty it
function manage_history_file {
    if [ ! -f $HISTORY_FILE ]; then
        echo -e "${RED}create $HISTORY_FILE"
        touch $HISTORY_FILE
    else 
        echo "" >> $HISTORY_FILE
    fi
}

function remove_history {
    if [ -f $HISTORY_FILE ]; then
        rm $HISTORY_FILE
        echo -e "${GREEN}historique supprimé (fichier $HISTORY_FILE supprimé)${NC}"
    fi
}

function write_in_history() {
    manage_history_file
    echo $1 >> $HISTORY_FILE
}

function manage_error_file {
    if [ -f $ERROR_FILE ]; then
        rm $ERROR_FILE
    else
        touch $ERROR_FILE
    fi
}

function print_last_errors {
    if [ -s $ERROR_FILE ]; then
        echo -e "${RED}erreurs survenus lors de la dernière synchronisation :${NC}" >&2
        echo $( cat $ERROR_FILE ) >&2
    fi
}

# use rsync to synchronize $1 in $2
# $1 = source
# $2 = destination
function rsync_interface() {
    echo -e "${GREEN}source : $1${NC}"
    echo -e "${GREEN}destination : $2${NC}\n"
    echo -e "${LIGHT_BLUE}début de la synchronisation${NC}"

    manage_history_file
    NOW=$(date +"%d/%m/%Y %H:%M")
    write_in_history "${NOW} : synchronisation de $1 avec $2 : "
    write_in_history ""
    manage_error_file
    rsync -a --out-format="%n%L" $1 $2 1>> $HISTORY_FILE 2> $ERROR_FILE
    write_in_history ""

    echo -e "${LIGHT_BLUE}fin de la synchronisation${NC} \n"

    echo ""
    print_last_errors
    echo ""
}

# print history of files synchronized during the last synchronization
function print_history {
    if [ -f $HISTORY_FILE ]; then
        echo ""
        echo "liste des fichiers transférés : "
        echo -e "$GREEN"
        less $HISTORY_FILE
        echo -e "$NC"
    fi
}

# init varialbe SRC from config file
function get_src_from_conf {
    # init SRC with value provided on the first line containing SOURCE= in CONFIG_GILE
    SRC=$(grep 'SOURCE' ${CONFIG_FILE} | cut -d '=' -f 2 | head -n 1)
}

# init varialbe DST from config file
function get_dst_from_conf {
    # init DST with value provided on the first line containing DESTINATION= in CONFIG_GILE
    DST=$(grep 'DESTINATION' ${CONFIG_FILE} | cut -d '=' -f 2 | head -n 1)
}

# main function who runs the syncrhonisation
function run_sync {
    # throw synchronisation
    rsync_interface $SRC $DST

    # if bidirectionnal argument given
    # throw synchronisation between destination and source
    if [[ $BIDIRECTIONAL == 1 ]]; then
        rsync_interface $DST $SRC
    fi

    echo -e "${GREEN}vous pouvez maintenant relancer le script avec la commande -p pour voir les fichiers qui ont été transférés$NC"
}

clear
NB_ARGS=$(( $# ))
echo -e "utiliser l'option -h pour visualiser l'aide \n"

SRC=""
DST=""
OTHER=""
BIDIRECTIONAL=0
CONFIG=0
echo
if [[ $NB_ARGS > 0 ]]; then
    while getopts "ecrbphs:d:" arg; do
        echo -e "option ${PURPLE}-$arg${NC} trouvée"
        case $arg in 
            h)
                help
                OTHER="help"
                ;;
            s)
                SRC=$OPTARG
                ;;
            d)
                DST=$OPTARG
                ;;
            p)
                print_history
                OTHER="history"
                ;;
            b)
                BIDIRECTIONAL=1
                ;;
            r)
                remove_history
                ;;
            c)
                CONFIG=1
                ;;
            e)
                print_errors
                ;;
            *)
                echo -e "${RED}cette option n'est pas disponible${NC}"
                ;;
        esac
    done
fi


if [[ $CONFIG == 1 ]]; then
    if [[ $SRC == "" ]]; then
        get_src_from_conf
        if [[ $SRC != "" ]]; then
            echo -e "${LIGHT_BLUE}utilisation du fichier de configuration pour intialiser le répertoire source${NC}"
        else
            echo -e "${RED}veuillez renseigner un répertoire source (-s) pour la synchronisation${NC}"
        fi
    fi

    if [[ $DST == "" ]]; then
        get_dst_from_conf
        if [[ $DST != "" ]]; then
            echo -e "${LIGHT_BLUE}utilisation du fichier de configuration pour intialiser le répertoire destination ${NC}"
        else
            echo -e "${RED}veuillez renseigner un répertoire destination (-d) pour la synchronisation${NC}"
        fi
    fi
fi


if [[ $SRC != "" ]] && [[ $DST != "" ]]; then
    run_sync
else
    echo 
    echo -e "pour effectuer une synchronisation renseigner un répertoire source (-s) ainsi qu'un répertoire destination (-d)"
fi
