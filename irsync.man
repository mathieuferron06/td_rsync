.\" Manpage pour irsync
.\" Contact ferron@et.esiea.fr
.TH man 5 "11 Novembre 2020" "1.0" "irsync man page"

.SH NOM
irsync \- synchronisation de repertoires

.SH SYNOPSIS
irsync -s [SOURCE] -d [DESTINATION]

.SH DESCRIPTION
irsync est un script bash permettant de synchroniser deux repertoires en utilisant l'executable rsync.

L'historique des synchronisations effectuees est stockees dans un fichier history.

Les options -s et -d permettent respectivement d'indiquer le repertoire source ainsi que le repertoire de destination de la synchronisation.

Il est possible d'utiliser un fichier de configuration pour déterminer les dossiers source et destination (voir option -c).

.SH OPTIONS
-h affiche l'aide contenu dans le script

-s [SOURCE] indique le repertoire source a synchroniser

-d [DESTINATION] indique le repertoire de destination (c'est celui qui sera synchronise avec [SOURCE])

-p affiche l'historique des synchronisations

-b permet de faire une synchronisation bidirectionnel, la synchronisation sera faite dans cet ordre : SOURCE -> DESTINATION puis SOURCE <- DESTINATION.

-r supprime le fichier contenant l'historique des synchronisations

-c utilise le fichier de configuration irsync.conf (doit se trouver dans le repertoire dans lequel le script irsync.sh est execute).
Dans ce fichier de configuration les variables SOURCE= et DESTINATION= permettent de definir les repertoires source et desintation.

.SH AUTRES RESSOURCES
man rsync
